# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import City, Organization, UserDetail, Groupdetail, SendRoleRequest, CountryInfo
from data.models import KeyDetail
from django.forms import ModelForm
from django.contrib.postgres.fields import HStoreField

# Register your models here.

admin.site.register(City)
admin.site.register(Organization)
admin.site.register(UserDetail)
admin.site.register(Groupdetail)
admin.site.register(SendRoleRequest)
admin.site.register(KeyDetail)
admin.site.register(CountryInfo)

# class DictionaryAdminForm(ModelForm):
#     content = HStoreField()
#     class Meta:
#         model = Dictionary
#         exclude = ()

# class DictionayAdmin(ModelForm):
#     form = DictionaryAdminForm
#     content = HStoreField()
